## Set up a Django Rest Project

1. install django and djangorestframework
2. create project: `django-admin startproject <project_name> .`
3. cd into project directory
4. create drf app: `django-admin startapp <app_name>`
5. Sync db: `python manage.py migrate`
6. Create admin user: `python manage.py createsuperuser --email admin@example.com --username admin`

## Create serializers

Simply put serializers help serialize native data objects to/from json.

1. Create a serializers module in <app_name>/serializers.py:
```python
from django.contrib.auth.models import User, Group
from rest_framework import serializers

... add serializers
```

## Add Views

DRF's APIView class subclasses Django's View class.

- request object is REST Framework's Request object type
- handler methods should return DRF Response objects
- incoming requests will be authenticaed/throttle-checked before calling handler

Create your views in `./<project_name>/<app_name>/views.py`

**FOLLOWUP:** Tutorial talks about adding ViewSets to share common behavior amongst views.

## Add URL endpoints

1. edit ./<project_name>/urls.py
2. rest\_framework.routers module allows for automatic creation of routes from ViewSets.
3. add api\_auth/ path for handling auth via browsable api. 


